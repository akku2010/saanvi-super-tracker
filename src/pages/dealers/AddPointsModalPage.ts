import { Component } from "@angular/core";
import { ViewController, NavParams, ToastController } from "ionic-angular";
import { ApiServiceProvider } from "../../providers/api-service/api-service";
@Component({
    // selector: 'page-modal',
    // templateUrl: './add-points-modal.html',
    template: `
    <div class="mainDiv">
    <div class="secondDiv">
        <ion-row>
            <ion-col style="padding: 0px; text-align: center;">
                <p style="margin: 0px; font-size:20px;">Add Points</p>
            </ion-col>
        </ion-row>
        <div class="temp">  
            <ion-label style="font-size: 1.2em;">Points</ion-label>
            <ion-item class="logitem" style="border: 1px solid black;">
                <ion-input [(ngModel)]="pointShareCount" type="number" (ngModelChange) ="calculateTotal()">
                </ion-input>
            </ion-item>
        </div>
        <p style="margin: 0px;color: #666; padding-left: 10px;padding-bottom: 2px;">
            Please type No. of points to be shared
        </p>
           
        <div class="temp">
            <ion-label style="font-size: 1.2em;">Price/Unit</ion-label>
            <ion-item class="logitem" style="border: 1px solid black;">
                <ion-input [(ngModel)]="perunitprice" type="number" (ngModelChange) ="calculateTotal()"></ion-input>
            </ion-item>
        </div>
        <p style="margin: 0px;color: #666; padding-left: 10px;padding-bottom: 10px;">
            Please type per unit price of points
        </p>
        <div class="temp">
            <ion-item>
                <ion-label>Payment Status</ion-label>
                <ion-select [(ngModel)]="payment_stauts">
                    <ion-option value="paid">Paid</ion-option>
                    <ion-option value="credit">Credit</ion-option>
                    <ion-option value="link">Send Payment Link</ion-option>
                </ion-select>
            </ion-item>
        </div>

        <div class="temp" *ngIf="payment_stauts === 'paid'">
            <ion-label style="font-size: 1.2em;">Remarks</ion-label>
            <ion-item class="logitem" style="border: 1px solid black;">
                <ion-input [(ngModel)]="remarks" type="text"></ion-input>
            </ion-item>
        </div>

        <div class="temp" *ngIf="payment_stauts === 'credit'">
            <ion-item class="logitem" style="border: 1px solid black;">
                <ion-datetime displayFormat="DD/MM/YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="credit_date"></ion-datetime>
            </ion-item>
        </div>
        <p *ngIf="payment_stauts === 'credit'" style="margin: 0px;color: #666; padding-left: 10px; padding-bottom: 10px;">
            Please Select Credit Date
        </p>
        <div class="temp" *ngIf="payment_stauts === 'link'">
           <ion-row>
                <ion-col col-8></ion-col>
                <ion-col col-2 style="padding: 0px;text-align: right;">
                    <ion-icon color="gpsc" style="font-size: 1.5em;" name="mail"></ion-icon>
                </ion-col>
                <ion-col col-2 style="padding: 0px;text-align: right;">
                    <ion-icon color="secondary" style="font-size: 1.8em;" name="text"></ion-icon>
                </ion-col>
           </ion-row>
        </div>
        <div class="temp">
            <p><span style="font-weight: bold;">Total Allocated Points</span> <span style="float: right;">{{totAllocatedPoints}}</span></p>
            <p><span style="font-weight: bold;">Total Available Points</span> <span style="float: right;">{{totAvailablePoints}}</span></p>
            <p><span style="font-weight: bold;">Total Amount</span> <span style="float: right;">{{totalAmount}}</span></p>
        </div>
      
        <div style="padding: 5px;">
            <ion-row>
                <ion-col col-5>
                    <button ion-button (click)="dismiss()" color="light" block>Cancle</button>
                </ion-col>
                <ion-col col-7>
                    <button ion-button (click)="proceed()" color="gpsc" block>Proceed</button>
                </ion-col>
            </ion-row>
        </div>
    </div>
</div>
    `,
    styles: [`
    .logitem {
        background-color: transparent;
        padding-bottom: 5px;

        ion-input {
            color: #000;
            ::placeholder {
                color: #000;
            }
        }
        border: 1px solid black;
        border-radius: 5px;
    }
  
    .mainDiv {
        padding: 15px;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.7);
    }

    .secondDiv {
        padding-top: 20px;
        border-radius: 18px;
        border: 2px solid black;
        background: white;
    }
    .temp {
        padding-left: 10px;
        padding-right: 10px;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ion-input {
        background-color: transparent;
    }
    .item-md.item-block .item-inner,
    .item-ios.item-block .item-inner {
        padding-right: 8px;
        border: none;
    }
        
    `]
})
export class AddPointsModalPage {
    min_time: any = "10";
    payment_stauts: string;
    perunitprice: number = 0;
    pointShareCount: number = 0;
    islogin: any;
    params: any = {};
    totAllocatedPoints: number = 0;
    totAvailablePoints: number = 0;
    totalAmount: number = 0;
    errmsg: string;

    constructor(
        private navParams: NavParams,
        private viewCtrl: ViewController,
        private apiCall: ApiServiceProvider,
        private toastCtrl: ToastController
    ) {
        this.params = this.navParams.get("params");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // debugger
        if (this.params.point_Allocated) {
            this.totAllocatedPoints = this.params.point_Allocated;
        }
    }
    dismiss() {
        this.viewCtrl.dismiss(this.min_time);
    }

    ionViewDidLoad() {
        this.checkPointsAvailability();
    }

    checkPointsAvailability() {
        var url = "https://www.oneqlik.in/users/checkPointAvailablity?u_id=" + this.islogin._id + "&demanded_points=0";
        this.apiCall.getSOSReportAPI(url)
            .subscribe((res) => {
                // console.log("points availability: ", res);
                if ((res.available_points == undefined) || (res.available_points < 0)) {
                    this.totAvailablePoints = 0;
                } else {
                    this.totAvailablePoints = res.available_points ? res.available_points : 0;
                }
            },
                err => {
                    console.log("error ", err);
                })
    }

    calculateTotal() {
        this.totalAmount = this.pointShareCount * this.perunitprice;
        // console.log("calculated amount: ", this.totalAmount)
    }

    proceed() {
        debugger
        var _bUrl = 'https://www.oneqlik.in/points/addpointAllocation';
        var insertPoint = {
            by: this.islogin._id,
            to: this.params._id,
            points: this.pointShareCount,
            rate: this.perunitprice,
            totalAmount: (this.pointShareCount * this.perunitprice),
            Payment_type: this.payment_stauts,
            created_date: new Date().toISOString(),
        }

        if (this.pointShareCount > this.totAvailablePoints) {
            this.errmsg = "Insufficient Points";
            this.showToast(this.errmsg);
            return;
        } else {
            if ((this.pointShareCount == undefined) || (this.payment_stauts == undefined)) {
                this.errmsg = "All Fields are mendatory";
                this.showToast(this.errmsg);
                return;
            } else {
                this.apiCall.startLoading().present();
                this.apiCall.urlpasseswithdata(_bUrl, insertPoint).subscribe((resp) => {
                    this.apiCall.stopLoading();
                    console.log("get response: ", resp);
                    if(resp.message) {
                        this.showToast(resp.message);
                        this.dismiss();
                    }
                },
                    err => {
                        this.apiCall.stopLoading();
                        console.log("error ", err);
                    });
            }
        }
    }

    showToast(msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'middle'
        }).present();
    }
}
