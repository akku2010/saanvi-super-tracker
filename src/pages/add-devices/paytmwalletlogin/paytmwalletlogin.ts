import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import * as moment from 'moment';
declare var RazorpayCheckout: any;

@IonicPage()
@Component({
  selector: 'page-paytmwalletlogin',
  templateUrl: 'paytmwalletlogin.html',
})
export class PaytmwalletloginPage {
  paytmmail: any;
  paytmnumber: any;
  paytmotp: number;
  successresponse: boolean = false;
  inputform: boolean = false;
  razor_key: string = 'rzp_live_jB4onRx1BUUvxt';
  paymentAmount: number = 150000;
  currency: any = 'INR';
  parameters: any;
  islogin: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public apiCall: ApiServiceProvider,
    private menu: MenuController,
    public toast: ToastController
  ) {
    // this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.islogin = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
    this.paytmnumber = this.islogin.phn;
    this.parameters = navParams.get('param');
    console.log("parameters", this.parameters)
  }
  ionViewDidEnter() {

    this.menu.enable(true);
  }

  paytmSignupOTPResponse: any;

  payWithRazor() {
    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/GO0jiDP.jpg',
      currency: this.currency, // your 3 letter currency code
      key: this.razor_key, // your Key Id from Razorpay dashboard
      amount: this.paymentAmount, // Payment amount in smallest denomiation e.g. cents for USD
      name: this.parameters.Device_Name,
      prefill: {
        email: this.islogin.email,
        contact: this.paytmnumber,
        name: this.islogin.fn + ' ' + this.islogin.ln
      },
      theme: {
        color: '#d80622'
      },
      modal: {
        ondismiss: function () {
          console.log('dismissed')
        }
      }
    };

    var successCallback = function (payment_id) {
      alert('payment_id: ' + payment_id);
      this.updateExpDate();
    };

    var cancelCallback = function (error) {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }

  goBack() {
    this.navCtrl.pop();
  }

  updateExpDate() {
    var tempdate = new Date();
    tempdate.setDate(tempdate.getDate() + 365);
    var expDate = moment(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
    console.log("updated expiry date: ", expDate);
    var url = this.apiCall.mainUrl + 'devices/editDevMaster';
    var payload = {
      // _id: this.parameters.Device_ID,
      _id: this.parameters._id,
      expiration_date: new Date(expDate).toISOString()
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(url, payload)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log("respData of expiry date updation: ", respData);
        let toast = this.toastCtrl.create({
          message: 'Payment success!!',
          duration: 1000,
          position: 'middle'
        });
        toast.onDidDismiss(() => {
          this.navCtrl.pop();
        });
        toast.present();

      },
        err => {
          console.log("oops got error: ", err);
          this.apiCall.stopLoading();

        });
  }

}
