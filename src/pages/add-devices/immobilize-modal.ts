import { Component, ChangeDetectorRef } from "@angular/core";
import { ViewController, NavParams, ToastController, AlertController, Events, Platform } from "ionic-angular";
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { TranslateService } from "@ngx-translate/core";
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import { SMS } from "@ionic-native/sms";

@Component({
  selector: 'page-immobilize',
  templateUrl: './immobilize-modal.html'
})
export class ImmobilizeModelPage {
  min_time: any = "10";
  obj: any;
  dataEngine: any;
  DeviceConfigStatus: any;
  immobType: any;
  messages: any;
  checkedPass: string;
  islogin: any;
  respMsg: any;
  intervalID: any;
  commandStatus: any;

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private speechRecognition: SpeechRecognition,
    public translate: TranslateService,
    private toastCtrl: ToastController,
    private apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    private events: Events,
    private sms: SMS,
    private plt: Platform,
    private cd: ChangeDetectorRef
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    if (navParams.get("param") != null) {
      this.obj = this.navParams.get("param");
    }
    if (localStorage.getItem("AlreadyDimissed") !== null) {
      localStorage.removeItem("AlreadyDimissed");
    }
  }
  matches: String[];
  isRecording = false;
  isIos() {
    return this.plt.is('ios');
  }

  stopListening() {
    this.speechRecognition.stopListening().then(() => {
      this.isRecording = false;

    });
  }

  getPermission() {
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        if (!hasPermission) {
          this.speechRecognition.requestPermission();
        }
      });
  }

  startListening() {
    let options = {
      language: 'en-US'
    }
    this.speechRecognition.startListening(options).subscribe(matches => {
      this.matches = matches;
      this.cd.detectChanges();
      console.log("matech length: ", this.matches.length);
      console.log("match found: ", this.matches[0])
      for (var i = 0; i < this.matches.length; i++) {
        if (this.matches[i] === 'Ignition lock' || this.matches[i] === 'Ignition unlock') {
          this.IgnitionOnOff(this.obj);
        }
      }
    });
    this.isRecording = true;
  }

  dismiss() {
    if (localStorage.getItem("AlreadyDimissed") !== null) {
      this.events.publish("Released:Dismiss");
      localStorage.removeItem("AlreadyDimissed");
    }
    console.log("Inside the function");
    this.viewCtrl.dismiss();
    localStorage.setItem("AlreadyDimissed", "true");
  }

  voiceToText() {
    // Check feature available
    this.speechRecognition.isRecognitionAvailable()
      .then((available: boolean) => console.log(available));

    // Check permission
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        console.log(hasPermission)
        if (!hasPermission) {
          // Request permissions
          this.speechRecognition.requestPermission()
            .then(
              () => {
                console.log('Granted');
                this.startListning();
              },
              () => console.log('Denied')
            )
        } else {
          let options = {
            // Android only
            showPartial: true
          }
          this.startListning();
        }
      });
  }

  startListning() {
    // Start the recognition process
    this.speechRecognition.startListening()
      .subscribe(
        (matches: Array<string>) => {
          console.log(matches)
          for (var i = 0; i < matches.length; i++) {
            if (matches[i] === 'ignition lock' || matches[i] === 'ignition unlock') {
              this.IgnitionOnOff(this.obj);
            }
          }
        },
        (onerror) => console.log('error:', onerror)
      );
  }

  unlockEngine() {
    let that = this;
    if (this.obj.last_ACC != null || this.obj.last_ACC != undefined) {

      if (localStorage.getItem('AlreadyClicked') !== null) {
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Process ongoing..'),
          duration: 1800,
          position: 'middle'
        });
        toast.present();
      } else {
        this.checkImmobilizePassword();
        this.messages = undefined;
        this.dataEngine = this.obj;
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
          .subscribe(data => {
            this.apiCall.stopLoading();
            this.DeviceConfigStatus = data;
            this.immobType = data[0].imobliser_type;
            var key;
            if (this.dataEngine.ignitionLock == '1') {
              this.messages = this.translate.instant('Do you want to unlock the engine?')
              key = 1;
            } else {
              if (this.dataEngine.ignitionLock == '0') {
                // this.messages = this.translate.instant('Do you want to lock the engine?')
                this.messages = this.translate.instant('The engine is already unlocked...!')
                key = 0;
              }
            }

            if (key === 1) {
              let alert = this.alertCtrl.create({
                message: this.messages,
                buttons: [{
                  text: 'YES',
                  handler: () => {
                    debugger
                    if (this.immobType == 0 || this.immobType == undefined) {
                      // that.clicked = true;
                      localStorage.setItem('AlreadyClicked', 'true');
                      var devicedetail = {
                        "_id": this.dataEngine._id,
                        "engine_status": !this.dataEngine.engine_status
                      }
                      // this.apiCall.startLoading().present();
                      this.apiCall.deviceupdateCall(devicedetail)
                        .subscribe(response => {
                          // this.apiCall.stopLoading();
                          // this.editdata = response;
                          const toast = this.toastCtrl.create({
                            message: response.message,
                            duration: 2000,
                            position: 'top'
                          });
                          toast.present();
                          // this.responseMessage = "Edit successfully";
                          // this.getdevices();

                          var msg;
                          if (!this.dataEngine.engine_status) {
                            msg = this.DeviceConfigStatus[0].resume_command;
                          }
                          else {
                            msg = this.DeviceConfigStatus[0].immoblizer_command;
                          }

                          this.sms.send(this.obj.sim_number, msg);
                          const toast1 = this.toastCtrl.create({
                            message: this.translate.instant('SMS sent successfully'),
                            duration: 2000,
                            position: 'bottom'
                          });
                          toast1.present();
                          // that.clicked = false;
                          localStorage.removeItem("AlreadyClicked");
                        },
                          error => {
                            // that.clicked = false;
                            localStorage.removeItem("AlreadyClicked");
                            // this.apiCall.stopLoading();
                            console.log(error);
                          });
                    } else {
                      console.log("Call server code here!!")
                      if (that.checkedPass === 'PASSWORD_SET') {
                        this.askForPassword(this.obj);
                        return;
                      }
                      that.serverLevelOnOff(this.obj);
                    }
                  }
                },
                {
                  text: this.translate.instant('NO')
                }]
              });
              alert.present();
            } else {
              let alert = this.alertCtrl.create({
                message: this.messages,
                buttons: ['Okay']
              });
              alert.present();
            }
          },
            error => {
              this.apiCall.stopLoading();
              console.log("some error: ", error._body.message);
            });
      }
    }
  }

  lockEngine() {
    let that = this;
    if (this.obj.last_ACC != null || this.obj.last_ACC != undefined) {

      if (localStorage.getItem('AlreadyClicked') !== null) {
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Process ongoing..'),
          duration: 1800,
          position: 'middle'
        });
        toast.present();
      } else {
        this.checkImmobilizePassword();
        this.messages = undefined;
        this.dataEngine = this.obj;
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
          .subscribe(data => {
            this.apiCall.stopLoading();
            this.DeviceConfigStatus = data;
            this.immobType = data[0].imobliser_type;
            var key;
            if (this.dataEngine.ignitionLock == '1') {
              // this.messages = this.translate.instant('Do you want to unlock the engine?')
              this.messages = this.translate.instant('The engine is already locked...!')

              key = 0;
            } else {
              if (this.dataEngine.ignitionLock == '0') {
                this.messages = this.translate.instant('Do you want to lock the engine?')
                // this.messages = this.translate.instant('The engine is already unlocked...!')
                key = 1;
              }
            }

            if (key === 1) {
              let alert = this.alertCtrl.create({
                message: this.messages,
                buttons: [{
                  text: 'YES',
                  handler: () => {
                    if (this.immobType == 0 || this.immobType == undefined) {
                      // that.clicked = true;
                      localStorage.setItem('AlreadyClicked', 'true');
                      var devicedetail = {
                        "_id": this.dataEngine._id,
                        "engine_status": !this.dataEngine.engine_status
                      }
                      // this.apiCall.startLoading().present();
                      this.apiCall.deviceupdateCall(devicedetail)
                        .subscribe(response => {
                          // this.apiCall.stopLoading();
                          // this.editdata = response;
                          const toast = this.toastCtrl.create({
                            message: response.message,
                            duration: 2000,
                            position: 'top'
                          });
                          toast.present();
                          // this.responseMessage = "Edit successfully";
                          // this.getdevices();

                          var msg;
                          if (!this.dataEngine.engine_status) {
                            msg = this.DeviceConfigStatus[0].resume_command;
                          }
                          else {
                            msg = this.DeviceConfigStatus[0].immoblizer_command;
                          }

                          this.sms.send(this.obj.sim_number, msg);
                          const toast1 = this.toastCtrl.create({
                            message: this.translate.instant('SMS sent successfully'),
                            duration: 2000,
                            position: 'bottom'
                          });
                          toast1.present();
                          // that.clicked = false;
                          localStorage.removeItem("AlreadyClicked");
                        },
                          error => {
                            // that.clicked = false;
                            localStorage.removeItem("AlreadyClicked");
                            // this.apiCall.stopLoading();
                            console.log(error);
                          });
                    } else {
                      console.log("Call server code here!!")
                      if (that.checkedPass === 'PASSWORD_SET') {
                        this.askForPassword(this.obj);
                        return;
                      }
                      that.serverLevelOnOff(this.obj);
                    }
                  }
                },
                {
                  text: this.translate.instant('NO')
                }]
              });
              alert.present();
            } else {
              let alert = this.alertCtrl.create({
                message: this.messages,
                buttons: ['Okay']
              });
              alert.present();
            }
          },
            error => {
              this.apiCall.stopLoading();
              console.log("some error: ", error._body.message);
            });
      }
    }
  }

  IgnitionOnOff(d) {
    let that = this;
    if (d.last_ACC != null || d.last_ACC != undefined) {

      if (localStorage.getItem('AlreadyClicked') !== null) {
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Process ongoing..'),
          duration: 1800,
          position: 'middle'
        });
        toast.present();
      } else {
        this.checkImmobilizePassword();
        this.messages = undefined;
        this.dataEngine = d;
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
          .subscribe(data => {
            this.apiCall.stopLoading();
            this.DeviceConfigStatus = data;
            this.immobType = data[0].imobliser_type;
            if (this.dataEngine.ignitionLock == '1') {
              this.messages = this.translate.instant('Do you want to unlock the engine?')
            } else {
              if (this.dataEngine.ignitionLock == '0') {
                this.messages = this.translate.instant('Do you want to lock the engine?')
              }
            }
            let alert = this.alertCtrl.create({
              message: this.messages,
              buttons: [{
                text: 'YES',
                handler: () => {
                  debugger
                  if (this.immobType == 0 || this.immobType == undefined) {
                    // that.clicked = true;
                    localStorage.setItem('AlreadyClicked', 'true');
                    var devicedetail = {
                      "_id": this.dataEngine._id,
                      "engine_status": !this.dataEngine.engine_status
                    }
                    // this.apiCall.startLoading().present();
                    this.apiCall.deviceupdateCall(devicedetail)
                      .subscribe(response => {
                        // this.apiCall.stopLoading();
                        // this.editdata = response;
                        const toast = this.toastCtrl.create({
                          message: response.message,
                          duration: 2000,
                          position: 'top'
                        });
                        toast.present();
                        // this.responseMessage = "Edit successfully";
                        // this.getdevices();

                        var msg;
                        if (!this.dataEngine.engine_status) {
                          msg = this.DeviceConfigStatus[0].resume_command;
                        }
                        else {
                          msg = this.DeviceConfigStatus[0].immoblizer_command;
                        }

                        this.sms.send(d.sim_number, msg);
                        const toast1 = this.toastCtrl.create({
                          message: this.translate.instant('SMS sent successfully'),
                          duration: 2000,
                          position: 'bottom'
                        });
                        toast1.present();
                        // that.clicked = false;
                        localStorage.removeItem("AlreadyClicked");
                      },
                        error => {
                          // that.clicked = false;
                          localStorage.removeItem("AlreadyClicked");
                          // this.apiCall.stopLoading();
                          console.log(error);
                        });
                  } else {
                    console.log("Call server code here!!")
                    if (that.checkedPass === 'PASSWORD_SET') {
                      this.askForPassword(d);
                      return;
                    }
                    that.serverLevelOnOff(d);
                  }
                }
              },
              {
                text: this.translate.instant('NO')
              }]
            });
            alert.present();
          },
            error => {
              this.apiCall.stopLoading();
              console.log("some error: ", error._body.message);
            });
      }
    }
  };

  checkImmobilizePassword() {
    const rurl = this.apiCall.mainUrl + 'users/get_user_setting';
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(rurl, Var)
      .subscribe(data => {
        if (!data.engine_cut_psd) {
          this.checkedPass = 'PASSWORD_NOT_SET';
        } else {
          this.checkedPass = 'PASSWORD_SET';
        }
      })
  }

  askForPassword(d) {
    const prompt = this.alertCtrl.create({
      title: 'Enter Password',
      message: "Enter password for engine cut",
      inputs: [
        {
          name: 'password',
          placeholder: 'Password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Proceed',
          handler: data => {
            console.log('Saved clicked');
            console.log("data: ", data)
            // if (data.password !== data.cpassword) {
            //   this.toastmsg("Entered password and confirm password did not match.")
            //   return;
            // }
            this.verifyPassword(data, d);
          }
        }
      ]
    });
    prompt.present();
  }

  verifyPassword(pass, d) {
    const ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
    var payLd = {
      "uid": this.islogin._id,
      "psd": pass.password
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(ryurl, payLd)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        console.log(resp);
        if (resp.message === 'password not matched') {
          this.toastmsg(resp.message)
          return;
        }
        this.serverLevelOnOff(d);
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  toastmsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    }).present();
  }

  serverLevelOnOff(d) {
    let that = this;
    // that.clicked = true;
    localStorage.setItem("AlreadyClicked", "true");
    var data = {
      "imei": d.Device_ID,
      "_id": this.dataEngine._id,
      "engine_status": d.ignitionLock,
      "protocol_type": d.device_model.device_type
    }
    // this.apiCall.startLoading().present();
    this.apiCall.serverLevelonoff(data)
      .subscribe(resp => {
        // this.apiCall.stopLoading();
        console.log("ignition on off=> ", resp)
        this.respMsg = resp;

        // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
        this.intervalID = setInterval(() => {
          this.apiCall.callResponse(this.respMsg._id)
            .subscribe(data => {
              console.log("interval=> " + data)
              this.commandStatus = data.status;

              if (this.commandStatus == 'SUCCESS') {
                clearInterval(this.intervalID);
                // that.clicked = false;
                localStorage.removeItem("AlreadyClicked");
                // this.apiCall.stopLoadingnw();
                const toast1 = this.toastCtrl.create({
                  message: this.translate.instant('process has been completed successfully!'),
                  duration: 1500,
                  position: 'bottom'
                });
                toast1.present();

                this.dismiss();
                // this.getdevices();
              }
            })
        }, 5000);
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error in onoff=>", err);
          localStorage.removeItem("AlreadyClicked");
          // that.clicked = false;
        });
  }

  onInfo() {
    const toast = this.toastCtrl.create({
      message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea voluptatibus quibusdam eum nihil optio, ullam accusamus magni, nobis suscipit reprehenderit, sequi quam amet impedit. Accusamus dolorem voluptates laborum dolor obcaecati.',
      showCloseButton: true,
      closeButtonText: 'Dismiss'
    });
    toast.present();
  }
}
